#!/usr/bin/env bash

source .env

FULL_RESTART="$1"
PROC_NAME="bin/www"

echo ""
echo -e "\n\n==========================="`date`"===========================\n\n"

d=" "

PID=`ps ax | grep "${APP_NAME}" | grep SCREEN | cut -b 1-5`
echo "CURRENT PID=${PID}"

if [ ! -z ${PID} ]
  then

    if [ ! -z ${FULL_RESTART} ]
      then
          echo -n "Killing process id "${PID}" ... "
          kill -9 ${PID}
          echo "done"
          sleep 2

          echo "Clear all screens ... "
          screen -wipe
          echo "Clearing of all screens DONE"
          sleep 2

          echo ""

      echo -n "Now starting new screen session ... "
      screen -A -m -d -S "${APP_NAME}" -X `npm run dev`
      sleep 2
      PID=`ps ax | grep ${APP_NAME} | grep SCREEN | cut -b 1-5`
      echo "done with PID="${PID}
    else
      echo "Process ${PID} is running. Restart NOT needed."
    fi

else
    echo -e "Empty, so start the session ... \n"
    screen -A -m -d -S "${APP_NAME}" -X "npm run dev"
    sleep 2
    PID=`ps ax | grep "${APP_NAME}" | grep SCREEN | cut -b 1-5`
    echo "done with PID="${PID}
fi

echo -e "\n==================================================================\n"
